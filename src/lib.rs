use core::fmt;
use sha::sha256;

type Entry<T> = (String, T);
type Slot<T> = Vec<Entry<T>>;
type Cell<T> = Option<Slot<T>>;

pub struct HashMap<V> {
    array: Vec<Cell<V>>,
    max_load_factor: f32,
    len: usize,
}

impl<V: std::fmt::Debug> fmt::Debug for HashMap<V> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_list().entries(self.array.iter()).finish()
    }
}

impl<V: std::fmt::Display> fmt::Display for HashMap<V> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut i = 0;
        write!(f, "{{")?;
        for cell in self.array.iter() {
            if let Some(slot) = cell {
                for entry in slot {
                    write!(f, "\n\t\"{}\": {},", entry.0, entry.1)?;
                    i += 1;
                }
            }
        }
        if i > 0 {
            writeln!(f, "")?;
        }
        write!(f, "}}")?;
        Ok(())
    }
}

impl<V> HashMap<V>
where
    V: Copy,
{
    pub fn new() -> Self {
        Self {
            array: vec![None; 20],
            max_load_factor: 0.7,
            len: 0,
        }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            array: vec![None; capacity],
            max_load_factor: 0.7,
            len: 0,
        }
    }

    pub fn insert(&mut self, key: &str, value: V) {
        // if max load factor is reached
        if (self.len + 1) as f32 / self.array.len() as f32 >= self.max_load_factor {
            self.rehash()
        }
        let index = Self::key_to_index(key, self.array.len());

        // get_mut returns a pointer to the mutable point in memory
        // it's safe to unwrap as long as `key_to_index`
        // returns index in range of `self.array.len()`
        let cell = self.array.get_mut(index).unwrap();
        let entry: Entry<V> = (key.to_string(), value);

        Self::put_in_cell(entry, cell);
        self.len += 1;
    }

    /// this is very expensive!!!
    fn rehash(&mut self) {
        let new_arr_len = self.array.len() * 2;
        let mut new_arr: Vec<Cell<V>> = vec![None; new_arr_len];

        for cell in self.array.iter() {
            if let Some(slot) = cell {
                for entry in slot {
                    let new_index = Self::key_to_index(&entry.0, new_arr_len);
                    new_arr[new_index] = Some(vec![entry.to_owned()]);
                }
            }
        }
        self.array = new_arr;
    }

    fn put_in_cell(entry: Entry<V>, cell: &mut Cell<V>) {
        let key = &entry.0;
        let value = &entry.1;

        if let Some(c) = cell {
            // check if key is already there
            if let Some(i) = Self::search(key, c) {
                // if it is then just change that value
                c[i].1 = *value;
            } else {
                // else push a new one
                c.push(entry);
            }
        } else {
            // if cell is empty -> initialize it with entry
            *cell = Some(vec![entry]);
        }
    }

    pub fn get(&self, key: &str) -> Option<&V> {
        let index = Self::key_to_index(key, self.array.len());

        if let Some(v) = self.array.get(index).unwrap() {
            if v.len() == 1 {
                Some(&v[0].1)
            } else {
                // if there are multiple items in that slot
                // it means there was a collision
                // we shall first check if the given key isn't already there
                if let Some(i) = Self::search(key, v) {
                    Some(&v[i].1)
                } else {
                    None
                }
            }
        } else {
            None
        }
    }

    /// returns index of a matching key in
    /// slot where there was collision
    fn search(key: &str, slot: &Slot<V>) -> Option<usize> {
        let mut i = 0;
        // TODO this could be some iterator magic
        // or maybe even binary search of sorts
        for (k, _) in slot {
            if k == key {
                return Some(i);
            }
            i += 1;
        }
        // if not found
        None
    }

    fn key_to_index(key: &str, len: usize) -> usize {
        let hash = sha256(key);
        let first_8_bytes: &str = &hash[0..8];
        let num: u32 = u32::from_str_radix(first_8_bytes, 16).unwrap();
        let index = num as usize % len;

        index
    }
}

#[cfg(test)]
mod hash_map_tests {
    use super::*;

    #[test]
    fn test_getting() {
        let mut hm = HashMap::<f64>::new();
        hm.insert("apple", 1.8);
        hm.insert("banana", 2.3);

        assert_eq!(hm.get("apple"), Some(&1.8));
        assert_eq!(hm.get("banana"), Some(&2.3));
    }

    #[test]
    fn test_missing_element() {
        let mut hm = HashMap::<f64>::new();
        hm.insert("apple", 1.8);
        hm.insert("cake", 7.8);

        assert_eq!(hm.get("banana"), None);
    }
    #[test]
    fn test_replacing() {
        let mut hm = HashMap::<f64>::new();
        hm.insert("apple", 1.8);
        hm.insert("banana", 2.3);

        hm.insert("apple", 2.7);
        assert_eq!(hm.get("apple"), Some(&2.7));
    }

    #[test]
    fn test_collisions() {
        let mut hm = HashMap::<f64>::new();
        hm.insert("honey", 5.4);
        hm.insert("jam", 3.2);

        // if there are 20 items then
        // "honey" and "jam" should be assigned the same index
        // therefore there is a collision
        assert_eq!(HashMap::<f64>::key_to_index("honey", 20), 16);
        assert_eq!(HashMap::<f64>::key_to_index("jam", 20), 16);

        assert_eq!(hm.get("honey"), Some(&5.4));
        assert_eq!(hm.get("jam"), Some(&3.2));
    }
    #[test]
    fn test_resizing() {
        let mut hm = HashMap::<f64>::with_capacity(5);
        assert_eq!(hm.array.len(), 5);

        hm.insert("apple", 1.8);
        hm.insert("banana", 2.3);
        hm.insert("cake", 7.8);
        hm.insert("donut", 3.8);

        assert_eq!(hm.array.len(), 10);
    }
}
