use hash_map::HashMap;
use std::io::{self, stdout, Write};
use std::str::FromStr;

fn prompt(p: &str) -> Option<String> {
    let mut buffer = String::new();
    print!("{}", p);
    stdout().flush().unwrap();
    if io::stdin().read_line(&mut buffer).unwrap() > 1 {
        return Some(buffer.strip_suffix("\n").unwrap().to_string());
    }
    None
}

fn main() -> Result<(), ()> {
    let mut hm = HashMap::<f64>::new();
    println!("Welcome to your store!");
    println!("What item would you like to add to your assortment?");
    println!("Empty input will allow you to access your items.");
    println!("");

    loop {
        let key: String;
        if let Some(k) = prompt("> ") {
            key = k;
        } else {
            break;
        }
        println!("\nAnd what will be its price? (in $)");

        let value: String;
        let price: f64;

        if let Some(v) = prompt("> ") {
            value = v;
        } else {
            print!("\nIt will be free then!");
            value = "0.0".to_string();
        }

        if let Ok(p) = f64::from_str(&value) {
            price = p;
        } else {
            eprintln!("That's not a valid number! It will be for free then!");
            price = 0.0;
        }
        hm.insert(&key, price);
        println!("\nNext item? Press enter to start accessing them.");
    }

    println!("\nassortment = {}", hm);

    println!("\nNow's the actual hash map demo.\nEnter item name to get its price:");
    if let Some(k) = prompt("> ") {
        if let Some(m) = hm.get(&k) {
            println!("{}'s price is ${}", k, m);
        } else {
            println!("No such key found!");
        }
    }
    return Ok(());
}
