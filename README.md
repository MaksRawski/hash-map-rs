# hash-map-rs
Hash map implementation in rust using [my own sha256 implementation](https://gitlab.com/MaksRawski/sha256-rs).

## Demo
Very abstract but at least you are free to add your own items and then try to get them.

```
cargo run -q
```

## Tests
An easier way to test that it actually works is to run unit tests available via:

```
cargo test
```
The implementation itself and tests can be found in `src/lib.rs`.

## LICENSE
Just like my earlier mentioned sha implementation, this entire repository is public domain (CC0). 
Do whatever you want with it!
